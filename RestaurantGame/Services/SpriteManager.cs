﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.Services
{
    public class SpriteManager : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        List<Sprite> spriteList = new List<Sprite>();

        public SpriteManager(Game game) : base(game)
        {

        }

        public override void Update(GameTime gameTime)
        {
            foreach (var sprite in spriteList)
            {
                sprite.Update(gameTime, Game.Window.ClientBounds);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            foreach (var sprite in spriteList)
            {
                sprite.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
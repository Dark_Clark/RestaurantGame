﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RestaurantGame.GameElements
{
    public abstract class Level
    {
        public GraphicsDeviceManager Graphics { get; }
        public ContentManager Content { get; }

        protected SoundEffect backgroundSound;

        protected SpriteFont font;

        protected List<Table> tables;

        public Point CursorPosition { get; set; }

        public int Score { get; set; }
        public int Lives { get; set; }

        public Table LayedTable { get; set; }

        protected Level(GraphicsDeviceManager graphics, ContentManager content,
            int lives = 3)
        {
            this.Graphics = graphics;
            this.Content = content;
            this.Lives = lives;
            this.Score = 0;

            CursorPosition = Point.Zero;
        }

        public virtual void LoadContent()
        {
            font = Content.Load<SpriteFont>("Fonts/JustFont");
            foreach (var table in tables)
            {
                table.LoadContent();
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            CursorPosition = Mouse.GetState().Position;
            foreach (var table in tables)
            {
                if (table.IsEmpty)
                    table.SpawnClient(5);
                table.Update(gameTime);
            }
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, CursorPosition.ToString(), new Vector2(10, 10), Color.Black);

            foreach (var table in tables)
            {
                table.Draw(gameTime, spriteBatch);
            }
        }

        public virtual void GameOver()
        {
            
        }
    }
}
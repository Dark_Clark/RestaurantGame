﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.GameElements
{
    public class GameObject
    {
        protected Texture2D texture;
        protected Vector2 position;

        protected Tuple<int, int> sheetSize; 
        protected Tuple<int, int> frameSize;
        protected Point currentFrame;
        protected float scale;

        public Rectangle Bounds =>new Rectangle(
            (int)position.X, (int)position.Y,
            (int) (frameSize.Item1 * scale),(int) (frameSize.Item2 * scale));

        protected Tuple<int, int> tileSize; 

        public GameObject(Vector2 position, Tuple<int, int> tileSize)
        {
            this.position = position;
            this.tileSize = tileSize;
        }

        public virtual void LoadContent()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }

        public float ScaleCalc()
        {
            return (Math.Min(texture.Height, texture.Width) / Math.Max(tileSize.Item1, tileSize.Item2));
        }
    }
}
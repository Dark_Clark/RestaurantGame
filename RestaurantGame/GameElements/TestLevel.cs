﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.GameElements
{
    public class TestLevel : Level
    {
        public TestLevel(GraphicsDeviceManager graphics, ContentManager content,
            int lives = 3) : base(graphics, content, lives)
        {
            tables = new List<Table>(2)
            {
                new Table(new Vector2(150, 200), this),
                new Table(new Vector2(600, 200), this)
            };
            tables[1].IsLayed = true;
            LayedTable = tables[1];
        }
    }
}
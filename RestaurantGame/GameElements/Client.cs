﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.GameElements
{
    public class Client : GameObject
    {
        private const float PatienceDecreaseSpeed = 5;

        private int timeSinceLastFrame;
        private int millisecondsPerFrame = 300;

        public Level Level { get; }

        private Table table;
        private readonly float eatingSpeed;
        private float patienceRemains;
        private float foodRemains;
        private ClientState currentState;

        public bool IsHappy { get; private set; }
        public bool IsLeaving { get; private set; }

        public ClientState CurrentState
        {
            get { return currentState; }
            set
            {
                if (value == ClientState.Waiting)
                {
                    currentFrame = new Point(2, 1);
                }
                currentState = value;
            }
        }

        public Client(Vector2 position, Level level,
                    Table table, float eatingSpeed = 5,
                    ClientState state = ClientState.Waiting)
            : base(position, new Tuple<int, int>(100,100))
        {
            Level = level;

            patienceRemains = 100;
            CurrentState = state;
            foodRemains = 100;
            IsHappy = false;
            IsLeaving = false;

            this.table = table;
            this.eatingSpeed = eatingSpeed;
        }

        public override void LoadContent()
        {
            texture = Level.Content.Load<Texture2D>("Sprites/man_eating_turkey_atlas");
            frameSize = new Tuple<int, int>(115, 111);
            sheetSize = new Tuple<int, int>(2,1);
            currentFrame = new Point(2,1);
        }

        public override void Update(GameTime gameTime)
        {
            switch (CurrentState)
            {
                case ClientState.Waiting:
                    if (patienceRemains > 0)
                    {
                        patienceRemains -=
                            PatienceDecreaseSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    else
                    {
                        IsHappy = false;
                        IsLeaving = true;
                        if (Level.Lives > 0)
                            Level.Lives--;
                        else
                            Level.GameOver();
                    }
                    break;
                case ClientState.Eating:

                    timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                    if (timeSinceLastFrame > millisecondsPerFrame)
                    {
                        timeSinceLastFrame = 0;
                        --currentFrame.X;
                        if (currentFrame.X >= sheetSize.Item1)
                        {
                            currentFrame.X = 2;
                            //++currentFrame.Y;
                            //if (currentFrame.Y >= sheetSize.Item2)
                            //    currentFrame.Y = 0;
                        }
                    }
                    if (foodRemains > 0)
                    {
                        patienceRemains = 100;
                        foodRemains -= eatingSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds; 
                    }
                    else
                    {
                        IsHappy = true;
                        IsLeaving = true;
                        Level.Score += 5;
                    }
                    break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, new Rectangle(
                currentFrame.X * frameSize.Item1,
                currentFrame.Y * frameSize.Item2,
                frameSize.Item1, frameSize.Item2),
                Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
        }
    }
}
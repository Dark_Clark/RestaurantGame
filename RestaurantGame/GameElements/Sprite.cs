﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.GameElements
{
    public class Sprite
    {
        protected Texture2D texture;
        protected Vector2 position;

        protected Tuple<int, int> sheetSize;
        protected Tuple<int, int> frameSize;
        protected Point currentFrame;
        protected float scale;

        protected Rectangle Bounds => new Rectangle(
            (int)position.X, (int)position.Y,
            (int)(frameSize.Item1 * scale), (int)(frameSize.Item2 * scale));

        protected Tuple<int, int> tileSize;

        public Sprite(Texture2D texture, Vector2 position, Tuple<int,int> sheetSize,
            Tuple<int,int> frameSize, Point currentFrame, float scale)
        {
            this.texture = texture;
            this.position = position;
            this.sheetSize = sheetSize;
            this.frameSize = frameSize;
            this.currentFrame = currentFrame;
            this.scale = scale;
        }
    }
}
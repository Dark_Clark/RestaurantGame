﻿using System;
using System.Diagnostics.Eventing.Reader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace RestaurantGame.GameElements
{
    public class Table : GameObject
    {
        private Client client;
        public Level Level { get; }

        public bool IsLayed { get; set; }

        public Client Client
        {
            get { return client; }
            private set { client = value; }
        }

        public bool IsEmpty => Client?.IsLeaving ?? true;

        public Table(Vector2 position, Level level) 
            : base(position, new Tuple<int, int>(100,100))
        {
            IsLayed = false;
            Level = level;
            scale = 0.2f;
        }

        public override void LoadContent()
        {
            texture = Level.Content.Load<Texture2D>("Sprites/table_round_blue");
            frameSize = new Tuple<int, int>(texture.Width, texture.Height);
            sheetSize = new Tuple<int, int>(1,1);
            currentFrame = new Point(1,1);
        }

        public override void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState();
            if (Bounds.Contains(mouseState.Position) && mouseState.LeftButton == ButtonState.Pressed)
            {
                Level.LayedTable.IsLayed = false;
                Level.LayedTable.Client.CurrentState = ClientState.Waiting;
                Level.LayedTable = this;
                IsLayed = true;
                Client.CurrentState = ClientState.Eating;
            }
            if (!IsEmpty)
            {
                Client.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {   
            spriteBatch.Draw(texture, position, null, Color.White, 0.0f, Vector2.Zero,
                            scale , SpriteEffects.None, 0.0f);
            if (!IsEmpty)
                Client.Draw(gameTime, spriteBatch);
            
        }

        public void SpawnClient(float clientEatingSpeed)
        {
            if (!IsEmpty) return;
            Client = new Client(new Vector2(position.X + 50, position.Y - 80), Level, this,
                clientEatingSpeed ,IsLayed ? ClientState.Eating : ClientState.Waiting);
            Client.LoadContent();
        }
    }
}
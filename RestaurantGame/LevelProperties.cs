﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RestaurantGame.GameLogic;

namespace RestaurantGame
{
    public class LevelProperties
    {
        public List<TableInfo> Tables { get; set; }
        public Texture2D BackgroundTexture { get; set; }
        public Sprite TableSprite { get; set; }
        public Sprite ClientSprite { get; set; }
        public Sprite ForkSprite { get; set; }
        public SpriteFont HudFont { get; set; }
        public double SpawnDelayTime { get; set; }
        public double TimeToDecreaseSpawnDelay { get; set; }
        public int StartClientEatSpeed { get; set; }
        public int ClientEatSpeedDelta { get; set; }
        public Point ClientWaitingFrame { get; set; }
        public Point ClientEatingFrame { get; set; }

        /// <summary>
        /// Game duration of this level (in seconds)
        /// </summary>
        public int LevelTime { get; set; }
        public int Lives { get; set; } = 3;
    }
}
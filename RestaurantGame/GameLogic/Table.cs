﻿using System;
using System.Diagnostics.Eventing.Reader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using RestaurantGame.Controller;

namespace RestaurantGame.GameLogic
{
    public class Table
    {
        public bool IsLayed { get; set; }

        public bool IsEmpty { get; set; }

        public Table(bool isLayed = false)
        {
            IsLayed = isLayed;
            IsEmpty = true;
        }
    }
}
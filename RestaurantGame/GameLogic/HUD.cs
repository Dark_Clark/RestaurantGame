﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using RestaurantGame.Controller;
using static System.Math;

namespace RestaurantGame.GameLogic
{
    public class HUD
    {
        public Level Level { get; }

        public int Score => Level.Score;
        public int Lives => Level.Lives;
        public int Timer => (int) Round(Level.Timer);

        public List<int> ClientsPatience
        {
            get 
            {
                var info = new List<int>(Level.Clients.Count);
                info.AddRange(Level.Clients.Select(client => (int)client.PatienceRemains));
                return info;
            }
        }

        public List<int> ClientsFood
        {
            get
            {
                var info = new List<int>(Level.Clients.Count);
                info.AddRange(Level.Clients.Select(client => (int)client.FoodRemains));
                return info;
            }
        }

        public HUD(Level level)
        {
            this.Level = level;
        }
    }
}
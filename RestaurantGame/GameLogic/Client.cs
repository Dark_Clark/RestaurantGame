﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RestaurantGame.Controller;
using RestaurantGame.GameLogic;

namespace RestaurantGame.GameLogic
{
    public class Client
    {
        private const float PatienceDecreaseSpeed = 5;

        public Level Level { get; }
        public int TableId { get; }

        private readonly float eatingSpeed;
        public float PatienceRemains { get; private set; }
        public float FoodRemains { get; private set; }

        public bool IsHappy { get; private set; }
        public bool IsLeaving { get; private set; }
        public bool IsEating => Level.Tables[TableId].IsLayed;

        public Client(Level level, int table, float eatingSpeed = 5) 
        {
            Level = level;
            TableId = table;
            this.eatingSpeed = eatingSpeed;

            IsHappy = false;
            IsLeaving = false;
            PatienceRemains = 100;
            FoodRemains = 100;
        }

        public void Update(GameTime gameTime)
        {
            switch (IsEating)
            {
                case false:
                    if (PatienceRemains > 0)
                    {
                        PatienceRemains -=
                            PatienceDecreaseSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    else
                    {
                        IsHappy = false;
                        IsLeaving = true;
                    }
                    break;
                case true:
                    if (FoodRemains > 0)
                    {
                        if (PatienceRemains < 100)
                            PatienceRemains += 3 * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        else
                            PatienceRemains = 100;
                        FoodRemains -= eatingSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds; 
                    }
                    else
                    {
                        IsHappy = true;
                        IsLeaving = true;
                    }
                    break;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}
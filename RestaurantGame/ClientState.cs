﻿namespace RestaurantGame
{
    public enum ClientState
    {
        Waiting, Eating
    }
}
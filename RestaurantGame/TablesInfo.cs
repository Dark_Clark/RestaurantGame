﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RestaurantGame.GameLogic;

namespace RestaurantGame
{
    public class TableInfo
    {
        public Table Table { get; }
        public Vector2 Position { get; set; }

        public TableInfo(Vector2 position, bool isLayed = false)
        {
            Table = new Table(isLayed);
            Position = position;
        }
    }
}
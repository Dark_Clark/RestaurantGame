﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RestaurantGame
{
    public class Sprite
    {
        public Texture2D TextureImage { get; }
        public Tuple<int, int> FrameSize { get; }

        public Tuple<int, int> SheetSize { get; }
        public int MillisecondsPerFrame { get; }
        public float Scale { get; }

        public int Width => (int) (TextureImage.Width * Scale);
        public int Height => (int) (TextureImage.Height * Scale);


        public Sprite(Texture2D textureImage,
            Tuple<int,int> frameSize, Tuple<int,int> sheetSize,
            int millisecondsPerFrame, float scale = 1.0f)
        {
            this.TextureImage = textureImage;
            Scale = scale;
            this.FrameSize = frameSize;
            this.SheetSize = sheetSize;
            this.MillisecondsPerFrame = millisecondsPerFrame;
        }

        public Sprite(Texture2D textureImage, float scale = 1.0f)
            : this(textureImage, Tuple.Create(textureImage.Width, textureImage.Height),
                  Tuple.Create(1, 1), 0, scale)
        { }
    }
}
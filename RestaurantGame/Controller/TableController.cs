﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RestaurantGame.GameLogic;
using RestaurantGame.Renderers;

namespace RestaurantGame.Controller
{
    public class TableController
    {
        private readonly TableRenderer renderer;
        private readonly Table model;

        //private Vector2 clientPositionOffset;

        private Vector2 TablePosition => renderer.Position;

        public Level Level { get; }

        public TableController(Table table, TableRenderer tableRenderer, Level level)
        {
            model = table;
            renderer = tableRenderer;
            Level = level;
        }

        public void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState();
            if (renderer.Bounds.Contains(mouseState.Position) && mouseState.LeftButton == ButtonState.Pressed)
            {
                Level.LayedTable = model;
            }
        }

        
    }
}
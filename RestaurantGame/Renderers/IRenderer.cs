﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.Renderers
{
    public interface IRenderer
    {
        void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
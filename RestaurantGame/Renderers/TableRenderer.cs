﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Table = RestaurantGame.GameLogic.Table;

namespace RestaurantGame.Renderers
{
    public class TableRenderer : IRenderer
    {
        private readonly Table model;

        public Vector2 Position { get; }
        public Vector2 ClientPositionOffset { get; set; }

        public Sprite TableSprite { get; }
        public Sprite ForkSprite { get;  }

        public Rectangle Bounds => new Rectangle(
            (int)Position.X, (int)Position.Y,
            TableSprite.TextureImage.Width, TableSprite.TextureImage.Height);

        public TableRenderer(Table table, Vector2 position, Sprite tableSprite, Sprite forkSprite)
        {
            model = table;
            Position = position;
            TableSprite = tableSprite;
            ForkSprite = forkSprite;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(TableSprite.TextureImage, Position, null, Color.White,
                0.0f, Vector2.Zero, TableSprite.Scale, SpriteEffects.None, 0.0f);
            if (model.IsLayed && model.IsEmpty)
            {
                var forkPosition = new Vector2(Position.X + TableSprite.Width / 2.5f, Position.Y + TableSprite.Height / 8); 
                spriteBatch.Draw(ForkSprite.TextureImage, forkPosition, null, Color.White, 0.0f, Vector2.Zero, ForkSprite.Scale, SpriteEffects.None, 0.0f);
            }
        }
    }
}
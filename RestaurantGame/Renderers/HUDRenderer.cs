﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RestaurantGame.Controller;
using RestaurantGame.GameLogic;

namespace RestaurantGame.Renderers
{
    public class HUDRenderer : IRenderer
    {
        private readonly HUD model;
        private readonly SpriteFont font;
        private readonly SpriteFont bigFont;
        private readonly Texture2D closedSign;

        private readonly Vector2 livesPosition;
        private readonly Vector2 scorePosition;
        private readonly Vector2 timerPosition;
        private readonly Vector2 signPosition;

        private readonly Vector2 clientInfoOffset;
        private readonly int space;

        private readonly Color textColor;

        public HUDRenderer(HUD hud,
            Vector2 livesPosition, Vector2 scorePosition, Vector2 timerPosition,
            Vector2 clientInfoOffset, SpriteFont font, SpriteFont bigFont, Color textColor)
        {
            model = hud;
            this.livesPosition = livesPosition;
            this.scorePosition = scorePosition;
            this.timerPosition = timerPosition;

            this.clientInfoOffset = clientInfoOffset;
            this.font = font;
            this.bigFont = bigFont;
            this.textColor = textColor;
            space = 70;

            closedSign = model.Level.Game.Content.Load<Texture2D>("Sprites/closed");
            signPosition = new Vector2(10, 150);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var screenWidth = model.Level.Game.Graphics.GraphicsDevice.Viewport.Width;
            var screenHeight = model.Level.Game.Graphics.GraphicsDevice.Viewport.Height;

            const int startX = 10;
            const int startY = 10;

            var livesString = $"Lives: {model.Lives}";
            var scoreString = $"$ {model.Score}";
            var timerString = $"Time: {model.Timer}";

            var livesPosition = new Vector2(startX, startY);
            var scorePosition  = new Vector2(livesPosition.X + 150, livesPosition.Y);
            var timerPosition = new Vector2(screenWidth/2, startY);

            spriteBatch.DrawString(font, livesString, livesPosition, textColor);
            spriteBatch.DrawString(font, scoreString, scorePosition, textColor);
            spriteBatch.DrawString(font, timerString, timerPosition, textColor);

            if (model.Level.RestaurantClosed)
            {
                spriteBatch.Draw(closedSign, signPosition, null, Color.White, 0f, Vector2.Zero, 0.3f, SpriteEffects.None, 0f);
            }

            var clRenderers = model.Level.ClientRenderers;

            for (int i = 0; i < clRenderers.Count; i++)
            {
                spriteBatch.DrawString(font, $"P: {model.ClientsPatience[i]}",
                    new Vector2(clRenderers[i].Position.X + clientInfoOffset.X,
                        clRenderers[i].Position.Y + clientInfoOffset.Y), textColor);
                spriteBatch.DrawString(font, $"F: {model.ClientsFood[i]}",
                    new Vector2(clRenderers[i].Position.X + clientInfoOffset.X + space,
                        clRenderers[i].Position.Y + clientInfoOffset.Y), textColor);
            }

            if (model.Level.GameStopped)
            {
                var pos = new Vector2(screenWidth / 2, screenHeight / 2);
                var endStr = model.Level.Win ?
                        "LEVEL COMPLETED!" 
                        : "LEVEL FAILED!";
                spriteBatch.DrawString(bigFont, endStr, pos, Color.White);
            }
        }
    }
}
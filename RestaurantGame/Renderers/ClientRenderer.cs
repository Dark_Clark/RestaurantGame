﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Client = RestaurantGame.GameLogic.Client;

namespace RestaurantGame.Renderers
{
    public class ClientRenderer : IRenderer
    {
        public Client Model { get; }

        private int timeSinceLastFrame;
        private readonly Point waitingStartFrame;
        private readonly Point eatingStartFrame;

        private Point currentFrame;
        public Vector2 Position { get; }

        public Sprite ClientSprite { get; }

        public Rectangle Bounds => new Rectangle(
            (int)Position.X, (int)Position.Y,
            (int)(ClientSprite.FrameSize.Item1 * ClientSprite.Scale),
            (int)(ClientSprite.FrameSize.Item2 * ClientSprite.Scale));

        public ClientRenderer(Client client, Vector2 position, Sprite clientSprite,
            Point waitingStartFrame, Point eatingStartFrame)
        {
            Model = client;
            Position = position;
            ClientSprite = clientSprite;
            this.waitingStartFrame = waitingStartFrame;
            this.eatingStartFrame = eatingStartFrame;
            currentFrame = waitingStartFrame;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            UpdateCurrentFrame(gameTime);
            spriteBatch.Draw(ClientSprite.TextureImage, Position , new Rectangle(
                currentFrame.X * ClientSprite.FrameSize.Item1,
                currentFrame.Y * ClientSprite.FrameSize.Item2,
                ClientSprite.FrameSize.Item1, ClientSprite.FrameSize.Item2),
                Color.White, 0, Vector2.Zero, ClientSprite.Scale, SpriteEffects.None, 0);
        }

        private void UpdateCurrentFrame(GameTime gameTime)
        {
            switch (Model.IsEating)
            {
                case false:
                    currentFrame = waitingStartFrame;
                    break;
                case true:
                    timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
                    if (timeSinceLastFrame > ClientSprite.MillisecondsPerFrame)
                    {
                        timeSinceLastFrame = 0;
                        ++currentFrame.X;
                        if (currentFrame.X >= ClientSprite.SheetSize.Item1)
                        {
                            currentFrame.X = 0;
                            ++currentFrame.Y;
                            if (currentFrame.Y >= ClientSprite.SheetSize.Item2)
                                currentFrame.Y = 0;
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
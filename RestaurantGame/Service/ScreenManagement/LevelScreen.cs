﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RestaurantGame.Controller;

namespace RestaurantGame.Service.ScreenManagement
{
    public class LevelScreen : GameScreen
    {
        private Level level;

        public LevelScreen(Level level)
        {
            this.level = level;
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            level.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            level.Draw(gameTime, spriteBatch);
        }
    }
}
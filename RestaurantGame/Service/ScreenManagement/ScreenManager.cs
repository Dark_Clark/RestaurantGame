﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RestaurantGame.Service.ScreenManagement
{
    public class ScreenManager
    {
        private Game game;

        private static ScreenManager instance;

        public static ScreenManager Instance =>
            instance ?? (instance = new ScreenManager());

        public ContentManager Content { get; private set; }
        public Vector2 Dimensions { get; private set; }
        public GameScreen CurrentScreen { get; set; }

        private ScreenManager()
        {
            Dimensions = new Vector2(1280, 768);
        }

        public void LoadContent(ContentManager content)
        {
            Content = new ContentManager(content.ServiceProvider, "Content");
            CurrentScreen.LoadContent();
        }

        public void UnloadContent()
        {
            CurrentScreen.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            CurrentScreen.Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            CurrentScreen.Draw(gameTime, spriteBatch);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RestaurantGame.Service
{
    public class InputManager
    {
        private KeyboardState currKeyboardState, prevKeyboardState;
        private MouseState currMouseState, prevMouseState;

        private readonly Dictionary<MouseButtons, Func<MouseState, ButtonState>> mouseButtonsStates; 

        private static InputManager instance;

        public static InputManager Instance =>
            instance ?? (instance = new InputManager());

        public Point CursorPosition => currMouseState.Position;

        private InputManager()
        {
            mouseButtonsStates = new Dictionary<MouseButtons, Func<MouseState, ButtonState>>()
            {
                {MouseButtons.Left, s => s.LeftButton },
                {MouseButtons.Middle, s => s.RightButton },
                {MouseButtons.Right, s => s.RightButton }
            };
        }

        public void Update()
        {
            prevKeyboardState = currKeyboardState;
            prevMouseState = currMouseState;
            currKeyboardState = Keyboard.GetState();
            currMouseState = Mouse.GetState();
        }

        public bool KeyPressed(params Keys[] keys)
        {
            return keys.Any(key => currKeyboardState.IsKeyDown(key) && prevKeyboardState.IsKeyUp(key));
        }

        public bool KeyReleased(params Keys[] keys)
        {
            return keys.Any(key => currKeyboardState.IsKeyUp(key) && prevKeyboardState.IsKeyDown(key));
        }

        public bool MousePressed(MouseButtons button)
        {
            return mouseButtonsStates[button](currMouseState) == ButtonState.Pressed &&
                   mouseButtonsStates[button](prevMouseState) == ButtonState.Released;
        }

        public bool MouseReleased(MouseButtons button)
        {
            return mouseButtonsStates[button](currMouseState) == ButtonState.Released &&
                   mouseButtonsStates[button](prevMouseState) == ButtonState.Pressed;
        }
    }

    public enum MouseButtons
    {
        Left,
        Right,
        Middle
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RestaurantGame.Controller;
using RestaurantGame.GameLogic;
using RestaurantGame.Service.ScreenManagement;

namespace RestaurantGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class RestaurantGame : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager Graphics { get; }
        SpriteBatch spriteBatch;

        //private Level level;

        private Texture2D backGroundTexture;
        
        public RestaurantGame()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //Graphics.PreferredBackBufferWidth = 1280;
            //Graphics.PreferredBackBufferHeight = 768;
            Graphics.PreferredBackBufferWidth = (int)ScreenManager.Instance.Dimensions.X;
            Graphics.PreferredBackBufferHeight = (int)ScreenManager.Instance.Dimensions.Y;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            

            backGroundTexture = Content.Load<Texture2D>("Textures/wood_floor1");

            var clientTexture = Content.Load<Texture2D>("Sprites/man_eating_turkey_atlas");
            var tableTexture = Content.Load<Texture2D>("Sprites/table_round_blue");
            var forkTexture = Content.Load<Texture2D>("Sprites/knife_fork");
            var hudFont = Content.Load<SpriteFont>("Fonts/JustFont");

            var tables = new List<TableInfo>()
            {
                new TableInfo(new Vector2(150, 150), true),
                new TableInfo(new Vector2(550, 150)),
                new TableInfo(new Vector2(950,150)),
                new TableInfo(new Vector2(150, 400)),
                new TableInfo(new Vector2(550, 400)),
                new TableInfo(new Vector2(950, 400))
            };

            var clientSprite = new Sprite(clientTexture, Tuple.Create(115, 111), Tuple.Create(2, 1), 300);
            var tableSprite = new Sprite(tableTexture, 0.2f);
            var forkSprite = new Sprite(forkTexture, 0.15f);

            var spawnDelayTime = 5.0d;
            var timeToDecreaseSpawnDelay = 30.0d;
            var startClientEatSpeed = 5;
            var clientEatSpeedDelta = 1;
            var clientWaitingFrame = new Point(2,1);
            var clientEatingFrame = new Point(1,1);
            var levelTime = 60;

            var level = new Level(this, tables, backGroundTexture,
                    tableSprite,clientSprite, forkSprite, hudFont,
                    spawnDelayTime, timeToDecreaseSpawnDelay,
                    startClientEatSpeed, clientEatSpeedDelta,
                    new Point(2, 1), new Point(1, 1), levelTime);

            ScreenManager.Instance.CurrentScreen = new LevelScreen(level);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                        Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();

                // TODO: Add your update logic here
                
                ScreenManager.Instance.Update(gameTime);
                
                base.Update(gameTime);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
            {
                ScreenManager.Instance.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
